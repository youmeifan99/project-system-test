<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TestAll</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>7b2f9330-ecfa-492d-87ae-ccfebe243e17</testSuiteGuid>
   <testCaseLink>
      <guid>81727a6a-20c6-4277-bdd8-560f2b1bfb99</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login to account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>456341d1-d07e-4efc-b4c1-6f29b3e43a97</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Available products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d55954a4-1b5e-4e70-a98d-83129cf9d1f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add to cart</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9128f675-3da7-4180-9856-067bb4d2a5ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Selected products</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d8316f18-f3ff-4d34-9093-2d198f90e590</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Admin total transaction</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8fddd38-74a5-4f55-acde-f4874183cce4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Log out</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b479be95-bd13-499a-8411-8aec8ec39ed8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
