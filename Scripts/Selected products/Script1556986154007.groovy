import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://52.23.209.162:8099/')

WebUI.setText(findTestObject('Selected products object/input_Username_username'), 'user')

WebUI.setEncryptedText(findTestObject('Selected products object/input_Password_password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Selected products object/button_Login'))

WebUI.click(findTestObject('Selected products object/button_add to cart'))

WebUI.click(findTestObject('Selected products object/button_add to cart'))

WebUI.click(findTestObject('Selected products object/Carts'))

WebUI.verifyElementText(findTestObject('Selected products object/td_Garden'), 'Garden')

WebUI.verifyElementText(findTestObject('Selected products object/td_20000 THB'), '20,000 THB')

WebUI.verifyElementText(findTestObject('Selected products object/td_Banana'), 'Banana')

WebUI.verifyElementText(findTestObject('Selected products object/td_150 THB'), '150 THB')

WebUI.verifyElementText(findTestObject('Selected products object/p_Total price  20150 THB'), 'Total price: 20,150 THB')

WebUI.setText(findTestObject('Selected products object/input_Garden_amount'), '2')

WebUI.verifyElementText(findTestObject('Selected products object/p_Total price  40150 THB'), 'Total price: 40,150 THB')

WebUI.click(findTestObject('Selected products object/button_confirm'))

WebUI.closeBrowser()

