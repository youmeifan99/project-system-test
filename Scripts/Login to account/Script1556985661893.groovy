import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://52.23.209.162:8099/')

WebUI.setText(findTestObject('Login to account object/input_Username'), 'admin')

WebUI.setEncryptedText(findTestObject('Login to account object/input_Password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Login to account object/button_Login'))

WebUI.verifyElementText(findTestObject('Login to account object/Products'), 'Products')

WebUI.verifyElementText(findTestObject('Login to account object/TotalTransaction'), 'Total Transaction')

WebUI.click(findTestObject('Login to account object/button_Logout'))

WebUI.click(findTestObject('Login to account object/button_Login'))

WebUI.verifyElementText(findTestObject('Login to account object/input_Username'), '')

WebUI.verifyElementText(findTestObject('Login to account object/Username is required'), 'Username is required')

WebUI.verifyElementText(findTestObject('Login to account object/input_Password'), '')

WebUI.verifyElementText(findTestObject('Login to account object/Password is required'), 'Password is required')

WebUI.setText(findTestObject('Login to account object/input_Username'), 'user')

WebUI.setEncryptedText(findTestObject('Login to account object/input_Password'), 'tzH6RvlfSTg=')

WebUI.click(findTestObject('Login to account object/button_Login'))

WebUI.verifyElementText(findTestObject('Login to account object/Usernamepassword is incorrect'), 'Username/password is incorrect')

WebUI.setEncryptedText(findTestObject('Login to account object/input_Password'), '1/VWEm4uipk=')

WebUI.click(findTestObject('Login to account object/button_Login'))

WebUI.verifyElementText(findTestObject('Login to account object/Products'), 'Products')

WebUI.verifyElementText(findTestObject('Login to account object/Carts'), 'Carts')

WebUI.closeBrowser()

