import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('http://52.23.209.162:8099/')

WebUI.setText(findTestObject('Admin total transaction object/input_Username_username'), 'admin')

WebUI.setEncryptedText(findTestObject('Admin total transaction object/input_Password_password'), 'RAIVpflpDOg=')

WebUI.click(findTestObject('Admin total transaction object/button_Login'))

WebUI.click(findTestObject('Admin total transaction object/Total Transaction'))

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_Garden Papaya'), 'Garden, Papaya')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_20120 THB'), '20,120 THB')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_Banana Garden Banana Rambutan'), 'Banana, Garden, Banana, Rambutan')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_60570 THB'), '60,570 THB')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_Orange Garden'), 'Orange, Garden')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_20560 THB'), '20,560 THB')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_Garden Banana'), 'Garden, Banana')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_40150 THB'), '40,150 THB')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_Garden Banana'), 'Garden, Banana')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_40150 THB'), '40,150 THB')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_Garden'), 'Garden')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_20000 THB'), '20,000 THB')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_Garden'), 'Garden')

WebUI.verifyElementText(findTestObject('Admin total transaction object/td_20000 THB'), '20,000 THB')

WebUI.verifyElementText(findTestObject('Admin total transaction object/p_Total price  221550 THB'), 'Total price: 221,550 THB')

WebUI.closeBrowser()

